#!/usr/bin/env python2
import os, sys
import subprocess
import re


try:
    import pygtk

    pygtk.require("2.0")
except:
    pass
try:
    import gtk
except:
    print("GTK Not Availible")
    sys.exit(1)


class osx86:
    def __init__(self):
        self.builder = gtk.Builder()
        self.builder.add_from_file("osx86_gui.glade")

        handlers = {"gtk_main_quit": self.gtk_main_quit,
                    "on_main_show": self.on_main_show,
                    "on_combobox1_show": self.on_combobox1_show,
                    "on_button1_clicked": self.on_button1_clicked
        }

        self.builder.connect_signals(handlers)

        self.window = self.builder.get_object("main")
        self.window.show_all()

    def err(self, message):
        message = gtk.MessageDialog(None,
                                    gtk.GTK_DIALOG_DESTROY_WITH_PARENT,
                                    gtk.MESSAGE_ERROR,
                                    gtk.BUTTONS_CLOSE,
                                    message)

    # def checkcmd(self,cmd):
    #     return (os.system("type -P " + cmd) == 0)

    def on_combobox1_show(self, *args):
        return

    def on_button1_clicked(self, *args):
        return

    def fill_drivelist(self, combo):
        items_list = gtk.ListStore(str)

        command = ['udisks', '--enumerate']
        proc = subprocess.Popen(command,
                                universal_newlines=True,
                                shell=False,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
        )
        if proc.wait() != 0:
            self.err("udisks command missing")

        txt_output = proc.stdout.read()
        txt_output_filtered_list = []

        for line in txt_output.splitlines():
            m = re.search('(/org/freedesktop/UDisks/devices/)([a-z]{3})$', line)
            if m:
                txt_output_filtered_list.append(m.group(2))

        txt_output_filtered_list.sort()

        for drive in txt_output_filtered_list:
            items_list.append([drive, ])

        combo.set_model(items_list)

        cell = gtk.CellRendererText()
        combo.pack_start(cell, True)
        combo.add_attribute(cell, 'text', 0)

    def gtk_main_quit(self, *args):
        print ("Exiting...")
        gtk.main_quit(*args)

    def on_main_show(self, *args):
        combo = self.builder.get_object("combobox1")
        #combo = self.get_object("combobox1")
        self.fill_drivelist(combo)

    def main(self):
        gtk.main()


if __name__ == "__main__":
    run = osx86()
    run.main()#!/usr/bin/env python2
import os, sys
import subprocess
import re


try:
    import pygtk

    pygtk.require("2.0")
except:
    pass
try:
    import gtk
except:
    print("GTK Not Availible")
    sys.exit(1)


class osx86:
    def __init__(self):
        self.builder = gtk.Builder()
        self.builder.add_from_file("osx86_gui.glade")

        handlers = {"gtk_main_quit": self.gtk_main_quit,
                    "on_main_show": self.on_main_show,
                    "on_combobox1_show": self.on_combobox1_show,
                    "on_button1_clicked": self.on_button1_clicked
        }

        self.builder.connect_signals(handlers)

        self.window = self.builder.get_object("main")
        self.window.show_all()

    def err(self, message):
        message = gtk.MessageDialog(None,
                                    gtk.GTK_DIALOG_DESTROY_WITH_PARENT,
                                    gtk.MESSAGE_ERROR,
                                    gtk.BUTTONS_CLOSE,
                                    message)

    # def checkcmd(self,cmd):
    #     return (os.system("type -P " + cmd) == 0)

    def on_combobox1_show(self, *args):
        return

    def on_button1_clicked(self, *args):
        return

    def fill_drivelist(self, combo):
        items_list = gtk.ListStore(str)

        command = ['udisks', '--enumerate']
        proc = subprocess.Popen(command,
                                universal_newlines=True,
                                shell=False,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
        )
        if proc.wait() != 0:
            self.err("udisks command missing")

        txt_output = proc.stdout.read()
        txt_output_filtered_list = []

        for line in txt_output.splitlines():
            m = re.search('(/org/freedesktop/UDisks/devices/)([a-z]{3})$', line)
            if m:
                txt_output_filtered_list.append(m.group(2))

        txt_output_filtered_list.sort()

        for drive in txt_output_filtered_list:
            items_list.append([drive, ])

        combo.set_model(items_list)

        cell = gtk.CellRendererText()
        combo.pack_start(cell, True)
        combo.add_attribute(cell, 'text', 0)

    def gtk_main_quit(self, *args):
        print ("Exiting...")
        gtk.main_quit(*args)

    def on_main_show(self, *args):
        combo = self.builder.get_object("combobox1")
        #combo = self.get_object("combobox1")
        self.fill_drivelist(combo)

    def main(self):
        gtk.main()


if __name__ == "__main__":
    run = osx86()
    run.main()
#!/usr/bin/env python
import subprocess
import re
import sys
from gi._gobject import GObject

try:
	import pygtk

	pygtk.require("2.0")
except:
	pass
try:
	import gtk
except:
	print("GTK Not Available")
	sys.exit(1)


class osx86():
	def on_button_about_clicked(self, widget):
		self.about = gtk.AboutDialog()
		atext = gtk.Label("Made by SMX.\n")
		apic = gtk.Image()
		self.about.vbox.pack_start(atext)
		self.about.show_all()
		self.about.run()
		self.about.hide()

	def err(self, message):
		err = gtk.MessageDialog(self, gtk.DIALOG_MODAL,
								gtk.MESSAGE_ERROR, gtk.BUTTONS_CLOSE, message)
		err.connect("response", self.quit)
		self.hide()
		result = err.run()

	def warn(self, message):
		warn = gtk.MessageDialog(self.container, gtk.DIALOG_MODAL,
								 gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, message)
		warn.run()
		warn.hide()

	def check_command(self, command):
		cmd = "type -P " + command + " &>/dev/null 2>&1"
		result = subprocess.call(["type", "-P", command], shell=True)
		return result

	def startup(self, widget):
		self.on_button_refresh_clicked(drivelist)

	def on_chk_show_all_toggled(self, widget):
		if widget.get_active():
			self.warn("Warning, Installing on your Hard Drive will wipe ALL DATA!")
		self.drivelist.emit("sort-column-changed")

	def on_rbutton_clicked(self, radiobutton):
		try:
			assert isinstance(radiobutton, gtk.RadioButton)
			if radiobutton.get_label() == "Use Drive":
				self.combo_choose_drv.set_sensitive(True)
			else:
				self.combo_choose_drv.set_sensitive(False)
		except:
			print "Class is not gtk.RadioButton"
			
	def on_button_choose_img_clicked(self, widget):
		self.dlg_file_chooser.show_all()
		
	def on_window_destroy(self, window):
		gtk.main_quit()
		
	def on_button_refresh_clicked(self, widget):
		print "On combo"
		showfixed = self.chk_show_all.get_active()
		self.drivelist.clear()
		if self.check_command("udisks"):
			self.err("udisks command missing!")
		disks = subprocess.Popen(["udisks", "--enumerate"], stdout=subprocess.PIPE).stdout.readlines()
		for line in disks:
			line = subprocess.Popen(["basename", line], stdout=subprocess.PIPE).stdout.readlines()[0].replace('\n', '')
			r = re.compile(".*[0-9]")
			if not r.match(line):
				test=open("/sys/block/"+line+"/removable", "r").read()[0]
				isrem=bool(int(open("/sys/block/"+line+"/removable", "r").read()[0]))
				print line+" Is Removable: "+str(isrem)
				if isrem or showfixed:
					ud = subprocess.Popen(["udisks", "--show-info", "/dev/" + line], stdout=subprocess.PIPE)
					grep = subprocess.Popen(["grep", "model"], stdin=ud.stdout, stdout=subprocess.PIPE)
					ud.stdout.close()  # make sure we close the output so p2 doesn't hang waiting for more input
					model = str(grep.communicate()[0]).replace('\n', '').replace('\t', '').replace('model:',
																								   '').lstrip()  # run our commands
					self.drivelist.append(["/dev/" + line + " - " + model, ])

	def __init__(self):
		self.builder = gtk.Builder()
		self.builder.add_from_file("osx86_gui.glade")

		handlers = {"on_window_destroy": self.on_window_destroy,
					"on_button_about_clicked": self.on_button_about_clicked,
					"on_button_refresh_clicked": self.on_button_refresh_clicked,
					"on_button_about_clicked": self.on_button_about_clicked,
					"on_rbutton_clicked":self.on_rbutton_clicked,
					"on_chk_show_all_toggled":self.on_chk_show_all_toggled,
					"on_button_choose_img_clicked":self.on_button_choose_img_clicked
				  
		}
		self.populate = self.on_button_refresh_clicked
		self.builder.connect_signals(handlers)
		
		### Containers and Special ##########################
		self.table = self.builder.get_object("table")
		self.toolbar = self.builder.get_object("toolbar")
		self.container = self.builder.get_object("container")
						
		#### ToolBar ###
		self.tbtn_about = self.builder.get_object("tbtn_about")
		self.tbtn_refresh = self.builder.get_object("tbtn_refresh")
		
		#### RadioBoxes #####
		self.rbutton_use_drive = self.builder.get_object("rbutton_use_drive")
		self.rbutton_use_img = self.builder.get_object("rbutton_use_img")
		self.rbutton_use_img.set_state(1)
		self.button_choose_img = self.builder.get_object("button_choose_img")
		self.button_choose_img

		##### Show All Drives CheckBox #####
		self.chk_show_all = self.builder.get_object("chk_show_all")

		####### Drives ComboBox ###########
		# assert isinstance("combo_choose_drv", gtk.ComboBox)
		self.combo_choose_drv = self.builder.get_object("combo_choose_drv")
		self.combo_cell = self.builder.get_object("combo_cell")
		
		####### File Chooser ##############
		self.dlg_file_chooser = self.builder.get_object("dlg_file_chooser")
		
		####### Set up ComboBox ############
		self.drivelist = gtk.ListStore(str)
		self.drivelist.connect("sort-column-changed", self.on_button_refresh_clicked)
		sorted = gtk.TreeModelSort(self.drivelist)
		self.combo_choose_drv.set_model(self.drivelist)
		combo_cell = gtk.CellRendererText()
		self.combo_choose_drv.pack_start(combo_cell, True)
		self.combo_choose_drv.add_attribute(combo_cell, 'text', 0)
		
		# Startup States of Widgets in Window #
		self.container.show_all()
		self.populate(self.combo_choose_drv)
		

	def quit(self, widget, *data):
		gtk.main_quit()


osx86()
gtk.main()

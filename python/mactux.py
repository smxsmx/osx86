#!/usr/bin/env python2
import os

try:  
    import pygtk
    pygtk.require("2.0")  
except:  
    pass  
try:  
    import gtk  
except:  
    print("GTK Not Availible")
    sys.exit(1)

class Handler:
	def on_window1_destroy(self, *args):
		print "Exiting..."
		gtk.main_quit(*args)

class osx86:
	def __init__(self):
		builder = gtk.Builder()
		builder.add_from_file("mactuxgui.glade")
		builder.connect_signals(Handler())
		window = builder.get_object("window1")
		window.show_all()
	def main(self):
		gtk.main()
	
if __name__ == "__main__":
	run = osx86()
	run.main()

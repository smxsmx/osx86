#!/bin/bash
set +e
echo "[-] $BASH_SOURCE"
export HOME=/root
export LC_ALL=C
trap 'echo "[+] $BASH_COMMAND"' DEBUG

XAR_VER="1.5.2"
D2I_VER="1.6.5"

MSYS_B="development"
MSYS_VER="2.3.0"

function add_sources(){
	add-apt-repository -y ppa:ubuntu-wine/ppa
}

function add_custom_packages(){
for debpack in ${EXTERNAL_DEBS[@]}; do
	local httppack=$(echo $debpack | grep -q "http://"; echo $?)
	local httpspack=$(echo $debpack | grep -q "https://"; echo $?)
	if [ $httppack == 0 ] || [ $httpspack == 0 ]; then
		echo "Web package, downloading..."
		if [ -f /tmp/$debpack ]; then rm /tmp/$(basename $debpack); fi		
		wget $debpack -O /tmp/$(basename $debpack)
		dpkg -i /tmp/$(basename $debpack)
	else
		dpkg -i $debpack
	fi
done
}

function addrem_packages(){
prev_ifs=$IFS
IFS=" "
REMOVE="${REMOVE[*]}"
INSTALL="${INSTALL[*]}"
BUILD_DEPS="${BUILD_DEPS[*]}"
IFS=$prev_ifs
apt-get update
yes | apt-get install -f
yes | apt-get purge $REMOVE
yes | apt-get install $INSTALL
if [ ! "$BUILD_DEPS" = "" ]; then
	yes | apt-get install $BUILD_DEPS
fi
yes | apt-get autoremove --purge
}

function upgradesys(){
apt-get update
yes | apt-get install -f
yes | apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade
yes | apt-get clean
}

function remove_builddeps(){
if [ ! "$BUILD_DEPS" = "" ]; then
	yes | apt-get purge $BUILD_DEPS
fi
}

function remove_customizations(){
cd /etc/linuxmint/adjustments
rm *firefox*
rm *chromium*
rm *opera*
cd /etc/skel
rm -r .mozilla
cd .config
rm -r chromium
}

function custom_theme(){
BACKGROUND="/usr/share/backgrounds/osx/tuxmac0.jpg"
BACK_STYLE="stretched"
CURSOR_THEME="DMZ-Black"
THEME="Clearlooks"
WINTHEME="ClearlooksRe"
ICON_THEME="osx"
local eskbackground=$(echo "file://$BACKGROUND" | sed 's/\ /\\\//g;s/\//\\\//g')

cd /usr/share/icons
cd default
rm index.theme
ln -s ../osx/index.theme index.theme

cd /usr/share/glib-2.0/schemas
sed "s/cursor-theme='DMZ-White'/cursor-theme='$CURSOR_THEME'/g" -i mint-artwork-mate.gschema.override

sed "s/gtk-theme='Mint-X'/gtk-theme='$WINTHEME'/g" -i mint-artwork-mate.gschema.override
sed "s/gtk-theme=\"Mint-X\"/gtk-theme=\"$WINTHEME\"/g" -i mint-artwork-mate.gschema.override

sed "s/icon-theme='Mint-X'/icon-theme='$ICON_THEME'/g" -i mint-artwork-mate.gschema.override
sed "s/icon-theme=\"Mint-X\"/icon-theme=\"$ICON_THEME\"/g" -i mint-artwork-mate.gschema.override

sed "s/theme='Mint-X'/theme='$THEME'/g" -i mint-artwork-mate.gschema.override
sed "s/theme=\"Mint-X\"/theme=\"$THEME\"/g" -i mint-artwork-mate.gschema.override

sed "s/picture-uri='\/usr\/share\/backgrounds\/linuxmint\/default_background.jpg'/picture-uri=\'$eskbackground\'/g" -i mint-artwork-mate.gschema.override
sed '/picture-uri/d' -i 10_ubuntu-settings.gschema.override
sed "s/picture-options=\"zoom\"/picture-options=\"$BACK_STYLE\"/g" -i mint-artwork-mate.gschema.override

glib-compile-schemas /usr/share/glib-2.0/schemas

if [ ! $(cat /usr/share/mate-panel/panel-default-layout.dist | grep -q "tuxmac"; echo $?) == 0 ]; then

cat <<EOF >> /usr/share/mate-panel/panel-default-layout.dist
[Toplevel top]
expand=true
orientation=top
size=24

[Object tuxmac-menu-bar]
object-type=menu-bar
toplevel-id=top
position=0
locked=true
EOF

fi
}

function add_opt(){
cd /opt/tuxmac
wget http://olarila.com/dsdt/DSDTEditor_Linux_Windows.zip
unzip -qu DSDTEditor_Linux_Windows.zip
rm DSDTEditor_Linux_Windows.zip
git clone https://github.com/smx-smx/osx86_linux.git
}

function set_path(){
EXTRA_PATH=""
for path in ${EXTRA_PATHS[@]}; do
	EXTRA_PATH=$EXTRA_PATH:$path
done
EXTRA_PATH=$EXTRA_PATH"\""
local p=$(cat /etc/environment | sed '$s/.$//')
p=$p$EXTRA_PATH
local espath=$(echo $p$EXTRA_PATH | sed -e 's/[\/&]/\\&/g')
sed "s/PATH=.*/$espath/g" -i /etc/environment
}

function enable_lang(){
echo "$1" >> /etc/locale.gen
locale-gen
}

source /packlist
source /pathlist

image_init "build"
enable_lang "it_IT.UTF-8 UTF-8"
add_sources
addrem_packages
add_custom_packages
remove_builddeps
remove_customizations
custom_theme
add_opt
set_path
update-pciids
update-usbids
upgradesys
image_deinit

#!/bin/bash
export PATH=$PATH:$(pwd -P)/build
export builddir=$(pwd -P)				#top level directory
export resdir=$builddir/resources		#resources folder
export imgroot=$builddir/image/edit		#target image folder		-> use this in image_chroot to copy staging files to target root
export stagingroot=$resdir/copyroot		#staging (copyroot) folder	-> use to prepare files before image_chroot
